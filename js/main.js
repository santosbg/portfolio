import * as THREE from './three/three.module.js';
import { OrbitControls } from './three/OrbitControls.js';
import { LineMaterial } from './three/LineMaterial.js';
import { Wireframe } from './three/Wireframe.js';

import { WireframeGeometry2 } from './three/WireframeGeometry2.js';
(() => {


  let wireframe, renderer, scene, camera, controls;
  let matLine;
  let insetWidth;
  let insetHeight;
  let sphere = $('#sphere');

  init();
  animate();

  function init() {

    renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true  } );
    renderer.setPixelRatio( sphere.devicePixelRatio );
    renderer.setClearColor( 0x000000, 0);
    renderer.setSize( sphere.offsetWidth, sphere.offsetHeight );
    sphere.append(renderer.domElement );

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(40, window.innerWidth/ window.innerHeight , 1, 1000 );
    camera.position.set( -50, 0, 50);

    controls = new OrbitControls( camera, renderer.domElement );
    controls.minDistance = 10;
    controls.maxDistance = 500;

    var geo = new THREE.IcosahedronBufferGeometry( 20, 1 );

    var geometry = new WireframeGeometry2( geo );

    matLine = new LineMaterial( {

      color: 0x000000,
      linewidth: 3,
      dashed: false

    } );

    wireframe = new Wireframe( geometry, matLine );
    wireframe.computeLineDistances();
    wireframe.scale.set( 1, 1, 1 );
    scene.add( wireframe );

    window.addEventListener( 'resize', onWindowResize, false );
    onWindowResize();
  }

  function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

    insetWidth = sphere.innerHeight / 4; // square
    insetHeight = sphere.innerHeight / 4;
  }

  function animate() {

    requestAnimationFrame( animate );

    // main scene

    renderer.setClearColor( 0x000000, 0 );

    renderer.setViewport( 0, 0, window.innerWidth, window.innerHeight );

    // renderer will set this eventually
    matLine.resolution.set( window.innerWidth, window.innerHeight ); // resolution of the viewport

    wireframe.rotation.x += 0.002;

    wireframe.rotation.y += 0.002;

    renderer.render( scene, camera );

    // inset scene

    renderer.setClearColor( 0x222222, 1 );

    renderer.clearDepth(); // important!

    renderer.setScissorTest( true );

    renderer.setScissor( 20, 20, insetWidth, insetHeight );

    renderer.setViewport( 20, 20, insetWidth, insetHeight );

    // renderer will set this eventually
    matLine.resolution.set( insetWidth, insetHeight ); // resolution of the inset viewport

    //renderer.render( scene, camera2 );

    renderer.setScissorTest( false );

  }

})();